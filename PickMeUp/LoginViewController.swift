//
//  LoginViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 04/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - IBOutlets
    
    // Email text field
    @IBOutlet weak var emailTextField: CustomTextField!
    // Password text field
    @IBOutlet weak var passwordTextField: CustomTextField!
    
    // MARK: - Constants
    // email
    let emailStored: String = "mario.rossi@pickmeup.com"
    // Password
    let passwordStored: String = "123123"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButtonWasPressed(_ sender: Any) {
        
        guard let email = emailTextField.text, !email.isEmpty else {
            showAlert(alertControllerTitle: "Email error", alertControllerMessage: "You must provide a valid email address", alertActionTitle: "OK")
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            showAlert(alertControllerTitle: "Password error", alertControllerMessage: "You must provide a valid password", alertActionTitle: "OK")
            return
        }
        
        if email == emailStored && password == passwordStored {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            self.present(tabBarController, animated: true, completion: nil)
        } else {
            showAlert(alertControllerTitle: "Wrong credentials", alertControllerMessage: "Email or password are not valid.", alertActionTitle: "OK")
            return
        }
    }
    
    @IBAction func signUpButtonWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "SignUpViewController", sender: nil)
    }
    
    // MARK: - Utils
    
    // Show alert
    func showAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
    
}

