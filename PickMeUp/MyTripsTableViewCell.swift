//
//  MyTripsTableViewCell.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 13/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class MyTripsTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    // Start location
    @IBOutlet weak var startLocationLabel: UILabel!
    // End location
    @IBOutlet weak var endLocationLabel: UILabel!
    // Number of passengers
    @IBOutlet weak var numberOfPassengersLabel: UILabel!
    // Date
    @IBOutlet weak var dateLabel: UILabel!
    // Driver
    @IBOutlet weak var driverLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(myTrip: MyTrip) {
        updateView(myTrip: myTrip)
    }
    
    func dateFormatter(myTrip: MyTrip) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = Formatter.Date.dayMonthHMin
        let timestamp = formatter.string(from: myTrip.tripDate)
        return timestamp
    }
    
    fileprivate func updateView(myTrip: MyTrip) {
        startLocationLabel.text = myTrip.startLocation
        endLocationLabel.text = myTrip.endLocation
        numberOfPassengersLabel.text = String(myTrip.numberOfPassengers)
        dateLabel.text = dateFormatter(myTrip: myTrip)
        driverLabel.text = myTrip.driver
    }

}
