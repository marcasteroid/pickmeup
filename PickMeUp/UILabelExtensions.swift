//
//  UILabelExtensions.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 05/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        }
        set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
}
