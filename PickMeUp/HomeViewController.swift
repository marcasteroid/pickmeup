//
//  HomeViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 12/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    // Start trip text field
    @IBOutlet weak var startTripTextField: CustomTextField!
    // Destination trip text field
    @IBOutlet weak var destinationTripTextField: CustomTextField!
    // Confirm trip button
    @IBOutlet weak var confirmTripButton: CustomButton!
    // Depart label static
    @IBOutlet weak var departLabelStatic: UILabel!
    // Return label static
    @IBOutlet weak var returnLabelStatic: UILabel!
    // Depart label dynamic
    @IBOutlet weak var departLabelDynamic: UILabel!
    // Return label dynamic
    @IBOutlet weak var returnLabelDynamic: UILabel!
    
    // MARK: - Properties
    // Start trip location
    var startTripLocation: String!
    // Destination trip location
    var destinationTripLocation: String!
    // Start trip date
    var startTripDate: String!
    // Destination trip date
    var destinationTripDate: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Start trip text field target action
        startTripTextField.addTarget(self, action: #selector(loadStartTripViewController), for: .touchDown)
        
        // Destination trip text field target action
        destinationTripTextField.addTarget(self, action: #selector(loadDestinationTripViewController), for: .touchDown)
        
        guard let startTripLocation = startTripTextField.text, !startTripLocation.isEmpty else {
            hideControls()
            return
        }
        
        guard let destinationTripLocation = destinationTripTextField.text, !destinationTripLocation.isEmpty else {
            hideControls()
            return
        }
        
        showControls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startTripTextField.text = startTripLocation
        destinationTripTextField.text = destinationTripLocation
        
        departLabelDynamic.text = startTripDate
        returnLabelDynamic.text = destinationTripDate
        
        guard let startTripLocation = startTripTextField.text, !startTripLocation.isEmpty else {
            hideControls()
            return
        }
        
        guard let destinationTripLocation = destinationTripTextField.text, !destinationTripLocation.isEmpty else {
            hideControls()
            return
        }
        
        showControls()
    }
        
    func showControls() {
        departLabelStatic.isHidden = false
        returnLabelStatic.isHidden = false
        departLabelDynamic.isHidden = false
        returnLabelDynamic.isHidden = false
        confirmTripButton.isHidden = false
    }
    
    func hideControls() {
        departLabelStatic.isHidden = true
        returnLabelStatic.isHidden = true
        departLabelDynamic.isHidden = true
        returnLabelDynamic.isHidden = true
        confirmTripButton.isHidden = true
    }
    
    // Load start trip view controller
    @objc func loadStartTripViewController() {
        performSegue(withIdentifier: "StartSegue", sender: nil)
    }
    
    // Load destination trip view controller
    @objc func loadDestinationTripViewController() {
        performSegue(withIdentifier: "DestinationSegue", sender: nil)
    }
    
    @IBAction func swapStartDestinationTextFieldWasTapped(_ sender: Any) {
        let startTripLocation = startTripTextField.text
        startTripTextField.text = destinationTripTextField.text
        destinationTripTextField.text = startTripLocation
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
