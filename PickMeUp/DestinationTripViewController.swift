//
//  DestinationTripViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 12/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class DestinationTripViewController: UIViewController {

    // MARK: - IBOutlets
    // Destination text field
    @IBOutlet weak var destinationTextField: CustomTextField!
    
    // Start trip location
    var startTripLocation: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonWasTapped(_ sender: Any) {
        
        guard let destinationLocation = destinationTextField.text, !destinationLocation.isEmpty else {
            showAlert(alertControllerTitle: "Destination missing", alertControllerMessage: "You must provide a valid destination", alertActionTitle: "OK")
            return
        }
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "DateViewController")
        self.present(tabBarController, animated: true, completion: nil)
        */
        performSegue(withIdentifier: "DateSegue", sender: self)

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DateSegue" {
            if let dateViewController = segue.destination as? DateViewController {
                dateViewController.startTripLocation = startTripLocation
                dateViewController.destinationTripLocation = destinationTextField.text
            }
        }

    }
    
    
    // MARK: - Utils
    
    // Show alert
    func showAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
}
