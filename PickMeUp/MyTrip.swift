//
//  MyTrip.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 13/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

class MyTrip {
    
    // Start location
    var startLocation: String!
    // End location
    var endLocation: String!
    // Number of passengers
    var numberOfPassengers: Int!
    // Trip date
    var tripDate: Date!
    // Driver
    var driver: String!
    
    init(startLocation: String, endLocation: String, numberOfPassengers: Int, tripDate: Date, driver: String) {
        self.startLocation = startLocation
        self.endLocation = endLocation
        self.numberOfPassengers = numberOfPassengers
        self.tripDate = tripDate
        self.driver = driver
    }
}
