//
//  Formatter.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 13/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct Formatter {
    struct Date {
        static let dayMonthHMin: String = "d MMMM, HH:mm"
    }
}
