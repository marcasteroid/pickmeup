//
//  MyTripsViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 13/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class MyTripsViewController: UIViewController {

    let myTrips = [
        MyTrip(startLocation: "Workplace", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "Ronald McMuffin"),
        MyTrip(startLocation: "Home", endLocation: "Workplace", numberOfPassengers: 3, tripDate: Date.init(), driver: "Gill Hylo"),
        MyTrip(startLocation: "Regent St.", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "John David"),
        MyTrip(startLocation: "Workplace", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "David Gilmour"),
        MyTrip(startLocation: "Wiki Sale", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "Philip Klyne"),
        MyTrip(startLocation: "Teather Park", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "Peter Marlow"),
        MyTrip(startLocation: "Workplace", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "Loren Huff"),
        MyTrip(startLocation: "Workplace", endLocation: "Home", numberOfPassengers: 3, tripDate: Date.init(), driver: "Andrew Red")
    ]
    
    // MARK: - IBOutlets
    // Table view
    @IBOutlet weak var myTripsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myTripsTableView.delegate = self
        myTripsTableView.dataSource = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MyTripsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myTrips.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "myTripsCell", for: indexPath) as? MyTripsTableViewCell {
            cell.configureCell(myTrip: myTrips[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
}
