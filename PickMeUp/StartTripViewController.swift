//
//  StartTripViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 12/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class StartTripViewController: UIViewController {

    // Location manager
    var locationManager: CLLocationManager?
    
    // MARK: - IBOutlets
    // Start trip text field
    @IBOutlet weak var startTripTextField: CustomTextField!
    @IBOutlet weak var oneWaySwitch: UISwitch!
    @IBOutlet weak var startLocationTableView: UITableView!
    
    // MARK: - Properties
    // Matching items
    var matchingItems: [MKMapItem] = [MKMapItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        
        startTripTextField.delegate = self
        
        // Check localization auth status
        checkLocationAuthStatus()
    }
    
    func checkLocationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            print("Hello world")
        } else {
            locationManager?.requestAlwaysAuthorization()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonWasTapped(_ sender: Any) {
        guard let startTripLocation = startTripTextField.text, !startTripLocation.isEmpty else {
            showAlert(alertControllerTitle: "Start location missing", alertControllerMessage: "You must provide a valid location", alertActionTitle: "OK")
            return
        }
        
        if oneWaySwitch.isOn {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            self.present(tabBarController, animated: true, completion: nil)
        } else {
            /*
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: "DestinationViewController")
            self.present(tabBarController, animated: true, completion: nil)
             */
            performSegue(withIdentifier: "DestinationSegue", sender: self)
        }
    }
    
    // MARK: - Utils
    
    // Show alert
    func showAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
    
    func performSearch() {
        matchingItems.removeAll()
        let request = MKLocalSearch.Request()
        var localRegion: MKCoordinateRegion

        let distance: CLLocationDistance = 3200

        request.naturalLanguageQuery = startTripTextField.text
        
        let currentLocation = CLLocationCoordinate2D.init(latitude: 51.509865, longitude: -0.118092)
        localRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: distance, longitudinalMeters: distance)

        request.region = localRegion

        let search = MKLocalSearch(request: request)

        
        search.start { (response, error) in
            if let error = error {
                self.showAlert(alertControllerTitle: "No location found", alertControllerMessage: error.localizedDescription, alertActionTitle: "OK")
            } else if response!.mapItems.count == 0 {
                self.showAlert(alertControllerTitle: "No results", alertControllerMessage: "Found no locations", alertActionTitle: "OK")
            } else {
                for mapItem in response!.mapItems {
                    self.matchingItems.append(mapItem as MKMapItem)
                    self.startLocationTableView.reloadData()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DestinationSegue" {
            if let destinationViewController = segue.destination as? DestinationTripViewController {
                destinationViewController.startTripLocation = startTripTextField.text!
            }
        }
    }
}


extension StartTripViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "startLocationCell", for: indexPath) as? StartLocationTableViewCell {
            let mapItem = matchingItems[indexPath.row]
            cell.configureCell(location: mapItem.name!, placemark: mapItem.placemark.title!)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapItem = matchingItems[indexPath.row]
        startTripTextField.text = mapItem.name! + " " + mapItem.placemark.title!
    }
}

extension StartTripViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            checkLocationAuthStatus()
        }
    }
}

extension StartTripViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == startTripTextField {
            startLocationTableView.delegate = self
            startLocationTableView.dataSource = self
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == startTripTextField {
            performSearch()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == startTripTextField {
            
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        matchingItems.removeAll()
        startLocationTableView.reloadData()
        return true
    }
}
