//
//  DateViewController.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 12/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class DateViewController: UIViewController {

    // MARK: - IBOutlets
    // Depart text field
    @IBOutlet weak var departTextField: CustomTextField!
    // Return text field
    @IBOutlet weak var returnTextField: CustomTextField!
    // Next button
    @IBOutlet weak var nextButton: CustomButton!
    
    // Depart date picker
    private var departDatePicker: UIDatePicker?
    // Return date picker
    private var returnDatePicker: UIDatePicker?
    
    // Start trip location
    var startTripLocation: String!
    // Destination trip location
    var destinationTripLocation: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Init depart date picker
        departDatePicker = UIDatePicker()
        departDatePicker?.datePickerMode = .dateAndTime
        departDatePicker?.addTarget(self, action: #selector(departDateChanged), for: .valueChanged)
        departDatePicker?.addTarget(self, action: #selector(hideNextButton), for: .touchUpInside)
        departTextField.inputView = departDatePicker
        
        // Init return date picker
        returnDatePicker = UIDatePicker()
        returnDatePicker?.datePickerMode = .dateAndTime
        returnDatePicker?.addTarget(self, action: #selector(returnDateChanged), for: .valueChanged)
        returnDatePicker?.addTarget(self, action: #selector(hideNextButton), for: .touchUpInside)
        returnTextField.inputView = returnDatePicker
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        // Do any additional setup after loading the view.
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TabBarController" {
            if let destinationViewController = segue.destination as? HomeViewController {
                destinationViewController.departLabelDynamic.text = departTextField.text
            }
        }
    }
 */
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        nextButton.isHidden = false
    }
    
    @objc func departDateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy @ hh:mm"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        departTextField.text = dateFormatter.string(from: datePicker.date)
        nextButton.isHidden = false
        view.endEditing(true)
    }
    
    @objc func returnDateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy @ hh:mm"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        returnTextField.text = dateFormatter.string(from: datePicker.date)
        nextButton.isHidden = false
        view.endEditing(true)
    }
    
    @objc func hideNextButton() {
        nextButton.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func nextButtonWasTapped(_ sender: Any) {
        guard let departDate = departTextField.text, !departDate.isEmpty else {
            showAlert(alertControllerTitle: "Depart date missing", alertControllerMessage: "You must provide a valid depart date", alertActionTitle: "OK")
            return
        }
        
        guard let returnDate = returnTextField.text, !returnDate.isEmpty else {
            showAlert(alertControllerTitle: "Return date missing", alertControllerMessage: "You must provide a valid return date", alertActionTitle: "OK")
            return
        }
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
        self.present(tabBarController, animated: true, completion: nil)
        */
        performSegue(withIdentifier: "HomeSegue", sender: self)

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeSegue" {
            if let homeViewController = segue.destination as? HomeViewController {
                homeViewController.startTripLocation = startTripLocation
                homeViewController.destinationTripLocation = destinationTripLocation
                homeViewController.startTripDate = departTextField.text
                homeViewController.destinationTripDate = returnTextField.text
            }
        }
        
    }
    
    // MARK: - Utils
    
    // Show alert
    func showAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
}
