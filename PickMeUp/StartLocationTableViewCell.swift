//
//  StartLocationTableViewCell.swift
//  PickMeUp
//
//  Created by Marco Margarucci on 13/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class StartLocationTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    // Start location
    @IBOutlet weak var startLocationLabel: UILabel!
    // Placemark
    @IBOutlet weak var placemarkLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(location: String, placemark: String) {
        updateView(location: location, placemark: placemark)
    }
    
    fileprivate func updateView(location: String, placemark: String) {
        startLocationLabel.text = location
        placemarkLabel.text = placemark
    }

}
