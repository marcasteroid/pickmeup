//
//  UI.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to UI

import Foundation
import UIKit

struct UI {
    
    struct Button {
        // No user account
        struct NoAccount {
            // First title
            static let firstTitle = "don't have an account ? "
            // Second title
            static let secondTitle = "sign up !"
            // First title color
            static let firstTitleColor: UIColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            // Second title color
            static let secondTitleColor: UIColor = #colorLiteral(red: 1, green: 0.3529411765, blue: 0, alpha: 1)
        }
        
        // Login/Logout button
        struct LoginLogout {
            // Login button title
            static let loginTitle: String = "login"
            // Logout button title
            static let logoutTitle: String = "logout"
        }
    }
    
    // Label
    struct Label {
        // Tag
        static let tag: Int = 1000
    }
    
    // Font
    struct Font {
        // Medium
        static let medium: String = "AvenirNext-Medium"
        // Regular
        static let regular: String = "AvenirNext-Regular"
        // Demi bold
        static let demiBold: String = "AvenirNext-DemiBold"
        // Size
        static let size: CGFloat = 12
    }
    
    // Text field
    struct TextField {
        // Plus sign
        static let plusSign: String = "+"
    }
    
    // Image
    struct Image {
        // White box
        static let whiteBox: String = "white-box"
    }
}
